import sys
sys.path.append('/home/user1/bddb')

import random
import unittest
from models.model import db, all_, count_
from models.gene import Gene
from models.geneSet import geneSlice, getGeneObjsSlice, multiGeneSignals, genes_by_genename, filtered_gene_objs

def show_gene_info(geneId):
    print "gene id: ", geneId
    return Gene(geneId)

def print_output(oneGene):
    print oneGene.entrezId, ":", oneGene.geneName, " => ", oneGene.description
    print "species: ", oneGene.species
    print "species # ", oneGene.speciesId
#    print "homolog:", Gene(oneGene.homologObj.entrez_gene_id)

    try:
        oneGene.homolog = Gene(oneGene.homologObj.entrez_gene_id)
        print "GENE: ", oneGene.homolog.geneName
        print "entrez ID: ", oneGene.homolog.entrezId
    except:
        print "no homolog"

#    jsonobj = oneGene.peaksForDisp
     
#    print "JSON: ", oneGene.peaksForDisp() #jsonobj

#    for peak in oneGene.peaks: #peakList:
#        print "peak: ", peak.peak_id, " for dataset ", peak.ds,  "and tss dist:" 
#        print peak.tss_dist, " bd_length-", peak.bd_length, 
#        print "Name: ", peak.peak_name,
#        print "method: ", peak.ds.method,
#        print "tissue: ", peak.ds.tissue, "=",peak.ds.tissue,
#        print "ACC: ", peak.ds.accession,"\n\n"

    return

#species_list = all_('species')

#for each in species_list:
#    print "SPECIES: ", each.species_id," ->",each.name

#geneByGeneName = show_gene_info("abca3")
#print_output(geneByGeneName)

oneHumanGene = show_gene_info(900) #900  #
print_output(oneHumanGene)
oneMouseGene = show_gene_info(18) #19657) #12450 #12
print_output(oneMouseGene)
geneByName = show_gene_info("serpina3")
print_output(geneByName)

gene_obj_arr = genes_by_genename('abca3') 
multiGeneJSON = multiGeneSignals(gene_obj_arr)

test_filter_dict = {"tissues": ["1","5","16"],
                    "methods": ["4","2"],
                    }

totalGenes = count_("genes")

print "TOTAL # genes ", totalGenes

print "# OF GENES FOR GENES BY GENENAME: ", len(gene_obj_arr)
print "JSON:", multiGeneJSON

for geneObject in gene_obj_arr:
    try:
        print_output(geneObject)
    except: 
        print "geneObject is not defined"

(filteredGenes, total) = filtered_gene_objs(test_filter_dict, 1, 10)

print "TOTAL #", total

for each in filteredGenes:
    print "gene: ", each.geneName

geneSlice = getGeneObjsSlice(0,10) #geneSlice(120,130)

geneRow =1

#geneObjList = list()

for geneObj in geneSlice:
    print "ROW: ", geneRow
    print "slice id: ", geneObj.entrezId
    print "gene name: ", geneObj.geneName
##    gene = Gene(geneObj.entrez_gene_id)
##    geneObjList.append(gene)
##    print_output(geneObj)
    geneRow = geneRow + 1


#geneListJSON = multiGeneSignals(geneSlice)

#print "JSON: ", geneListJSON

#print all_('methods')
#print all_('tissues')

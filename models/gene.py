################################################
# returns gene object and all relationships to 
# dataset, species, homology, etc. tables #
##############################################
from model import db #, gene_event_for_canvasXpress
from sqlalchemy.orm import exc
import json

class Gene():
    def __init__(self, query): # query can be geneName or geneId
        try:  # try as gene name first
            geneObj = \
                db.genes.filter(db.genes.symbol == query).one()

     #   except exc.MultipleResultsFound:
      #      return None
 
        except: # try with entrezid
            geneObj =  \
                db.genes.filter(db.genes.entrez_gene_id == query).one() 
     
        self.entrezId = geneObj.entrez_gene_id
        self.geneName = geneObj.symbol
        self.speciesId = geneObj.species_id
        self.species = geneObj.species.name
        self.peaks = geneObj.peaks # array

        ## define description
        try:
            self.description = geneObj.description 
        except:
            self.description = ""

     #   print "gene species: ", self.speciesId

        # define homologs
        if self.speciesId is 1: ## gene is mouse gene; want human homolog
            try:
                self.homologObj = geneObj.human_homologs.hgene[0]
            except:
                self.homologObj = {}
        elif self.speciesId is 2:   ## geme is a human gene; want mouse homolog
            try:
                self.homologObj = geneObj.mouse_homologs.mgene[0]
            except:
                self.homologObj = {}
         
        self.peaksConfig = json.dumps({"graphType" : "Heatmap",
                            "gradient" : "true",
                            "indicatorWidth" : 3,
                            "useFlashIE" : "true",
                             "xAxisTicks": 0,
                            "heatmapType": "red",
                            "centerData": "true"})

        ## returns click event as json object to open gene page for canvasXpress##
        self.peak_event = json.dumps({"click": 'function(o){alert("This is the new event");'})

    def peaksForDisp(self):  #returns a json object for canvasExpress
        variables = []
        data = []
        
        for eachPeak in self.peaks:
            dataset = eachPeak.ds.bio_sample
            variables.append(dataset)
            data.append([eachPeak.breadth_signal])
        
        jsonData = json.dumps({'y' : 
                               { 'vars' : variables, 
                                 'smps' : [self.geneName],
                                 'desc': ['Intensity'], 
                                 'data' : data}
                   })
        
        return jsonData

    def breadthByDsId(self): # returns a dict of breadth_signals by key=dataset id
        breadthByDs = {}
        
        for eachPeak in self.peaks:
            breadthByDs[str(eachPeak.ds.dataset_id)] = eachPeak.breadth_signal
            
        return breadthByDs

    def biosampleByDsId(self): # returns a dict of biosamples by key=dataset id
        biosamplesByDs = {}
        
        for eachPeak in self.peaks:
            biosamplesByDs[str(eachPeak.ds.dataset_id)] = str(eachPeak.ds.bio_sample)
            
        return biosamplesByDs
